import numpy as np

step = 0.001

# 函數 f 對變數 p[k] 的偏微分: df / dp[k]
def df(f, p, k):
    p1 = p.copy()
    p1[k] = p[k]+step
    return (f(p1) - f(p)) / step

# 函數 f 在點 p 上的梯度
def grad(f, p):
    gp = p.copy()
    for k in range(len(p)):
        gp[k] = df(f, p, k)
    return gp

# 限制：f 為 n 維向量函數 f(Rn)->Rn
def div(f, p):
    r = 0
    for i in range(p.size):
        r += df(f, p, i)[i]
    return r

# 限制：f 為 3 維向量函數 f(R3)->R3
def curl(f, p):
    rx = df(f, p, 1)[2]-df(f, p, 2)[1]
    ry = df(f, p, 2)[0]-df(f, p, 0)[2]
    rz = df(f, p, 0)[1]-df(f, p, 1)[0]
    return np.array([rx, ry, rz])

def laplace(f, p):
    return div(lambda p:grad(f, p), p)
