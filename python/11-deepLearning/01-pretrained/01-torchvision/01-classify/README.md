# Pretrained Model

來源 -- https://www.learnopencv.com/pytorch-for-beginners-image-classification-using-pre-trained-models/ 

## alexnet.py

```
mac020:pretrained mac020$ python3 alexnet.py
img_t.shape= torch.Size([3, 224, 224])
batch_t.shape= torch.Size([1, 3, 224, 224])
preds.shape= torch.Size([1, 1000])
Labrador retriever
```

## predict.py

```
(env) mac020:pretrained mac020$ python3 predict.py alexnet img/dog.jpg
img_t.shape= torch.Size([3, 224, 224])
batch_t.shape= torch.Size([1, 3, 224, 224])
preds.shape= torch.Size([1, 1000])
Labrador retriever

(env) mac020:pretrained mac020$ python3 predict.py alexnet img/cat.jpg
img_t.shape= torch.Size([3, 224, 224])
batch_t.shape= torch.Size([1, 3, 224, 224])
preds.shape= torch.Size([1, 1000])
Egyptian cat
```

